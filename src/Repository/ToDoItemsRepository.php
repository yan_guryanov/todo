<?php

namespace App\Repository;

use App\Entity\ToDoItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ToDoItemsRepository extends ServiceEntityRepository implements ToDoItemsRepositoryInterface
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ToDoItem::class);
    }

    /**
     * {@inheritdoc}
     */
    public function findAllForToday(): array
    {
        $qb = $this->createQueryBuilder('t');
        $qb
            ->where('t.createdForDate = :today')
            ->setParameter('today', (new \DateTime())->format('Y-m-d'));

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function save(ToDoItem $todoItem): void
    {
        $this->getEntityManager()->persist($todoItem);
        $this->getEntityManager()->flush();
    }
}
