<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ToDoItem;

interface ToDoItemsRepositoryInterface
{
    /**
     * @return ToDoItem[]
     *
     * @throws \Exception
     */
    public function findAllForToday(): array;

    /**
     * @param ToDoItem $todoItem
     */
    public function save(ToDoItem $todoItem): void;
}
