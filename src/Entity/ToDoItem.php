<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ToDoItemsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ToDoItemsRepository::class)
 * @ORM\Table(name="todo_items")
 */
class ToDoItem
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Groups({"details", "create"})
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @Serializer\Groups({"details"})
     */
    private $completed = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     *
     * @Serializer\Groups({"create"})
     * @Assert\NotBlank()
     */
    private $createdForDate;

    /**
     * @var string|null
     *
     * @Serializer\Groups({"list"})
     */
    private $completeUrl;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function isCompleted(): ?bool
    {
        return $this->completed;
    }

    public function markCompleted()
    {
        $this->completed = true;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @param \DateTime $createdForDate
     */
    public function setCreatedForDate(\DateTime $createdForDate): void
    {
        $this->createdForDate = $createdForDate;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedForDate(): \DateTime
    {
        return $this->createdForDate;
    }

    /**
     * @return string
     */
    public function getCompleteUrl(): string
    {
        return $this->completeUrl;
    }

    /**
     * @param string $completeUrl
     */
    public function setCompleteUrl(string $completeUrl): void
    {
        $this->completeUrl = $completeUrl;
    }
}
