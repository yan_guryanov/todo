<?php

declare(strict_types=1);

namespace App\Application;

use App\Application\ToDoApp\ToDoItemsCollection;
use App\Entity\ToDoItem;
use App\Repository\ToDoItemsRepositoryInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ToDoApp implements ToDoAppInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var ToDoItemsRepositoryInterface
     */
    private $todoItemsRepo;

    /**
     * @param ValidatorInterface           $validator
     * @param RouterInterface              $router
     * @param ToDoItemsRepositoryInterface $todoItemsRepo
     */
    public function __construct(
        ValidatorInterface $validator,
        RouterInterface $router,
        ToDoItemsRepositoryInterface $todoItemsRepo
    ) {
        $this->validator = $validator;
        $this->router = $router;
        $this->todoItemsRepo = $todoItemsRepo;
    }

    /**
     * {@inheritdoc}
     */
    public function getToDoList(): ToDoItemsCollection
    {
        $items = $this->todoItemsRepo->findAllForToday();

        foreach ($items as $item) {
            $url = $this->router->generate('todo-item-complete', ['id' => $item->getId()]);
            $item->setCompleteUrl($url);
        }

        return new ToDoItemsCollection($items);
    }

    /**
     * {@inheritdoc}
     */
    public function createToDoItem(ToDoItem $todoItem): ToDoItem
    {
        $this->validator->validate($todoItem);
        $this->todoItemsRepo->save($todoItem);

        return $todoItem;
    }

    /**
     * {@inheritdoc}
     */
    public function markToDoItemCompleted(ToDoItem $todoItem): void
    {
        $todoItem->markCompleted();
        $this->todoItemsRepo->save($todoItem);
    }
}
