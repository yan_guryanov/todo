<?php

namespace App\Application\ToDoApp;

use App\Entity\ToDoItem;
use Symfony\Component\Serializer\Annotation as Serializer;

class ToDoItemsCollection
{
    /**
     * @var ToDoItem[]
     *
     * @Serializer\Groups({"details"})
     */
    private $items;

    /**
     * @param ToDoItem[] $items
     */
    public function __construct(array $items)
    {
        $this->items = [];
        foreach ($items as $item) {
            $this->addToCollection($item);
        }
    }

    /**
     * @return ToDoItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param ToDoItem $item
     */
    private function addToCollection(ToDoItem $item)
    {
        $this->items[] = $item;
    }
}
