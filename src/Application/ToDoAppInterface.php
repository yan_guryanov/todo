<?php

declare(strict_types=1);

namespace App\Application;

use App\Application\ToDoApp\ToDoItemsCollection;
use App\Entity\ToDoItem;

interface ToDoAppInterface
{
    /**
     * @return ToDoItemsCollection
     */
    public function getToDoList(): ToDoItemsCollection;

    /**
     * @param ToDoItem $todoItem
     *
     * @return ToDoItem
     */
    public function createToDoItem(ToDoItem $todoItem): ToDoItem;

    /**
     * @param ToDoItem $todoItem
     */
    public function markToDoItemCompleted(ToDoItem $todoItem): void;
}
