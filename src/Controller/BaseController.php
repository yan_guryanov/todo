<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{
    /**
     * @Route(path="/", name="todo-list", methods={"get"})
     */
    public function index()
    {
        return $this->render('base.html.twig');
    }
}
