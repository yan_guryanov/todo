<?php

declare(strict_types=1);

namespace App\Controller;

use App\Application\ToDoAppInterface;
use App\Entity\ToDoItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route(path="/todo")
 */
class ToDoController extends AbstractController
{
    /**
     * @Route(path="", name="todo-list", methods={"get"})
     *
     * @param ToDoAppInterface $todoApp
     *
     * @return JsonResponse
     */
    public function index(ToDoAppInterface $todoApp)
    {
        $todoList = $todoApp->getToDoList();

        return $this->json($todoList, Response::HTTP_OK, [], ['groups' => ['details', 'list']]);
    }

    /**
     * @Route(path="", name="todo-item-create", methods={"post"})
     *
     * @param Request             $request
     * @param SerializerInterface $serializer
     * @param ToDoAppInterface    $todoApp
     *
     * @return JsonResponse
     */
    public function create(Request $request, SerializerInterface $serializer, ToDoAppInterface $todoApp)
    {
        /** @var ToDoItem $todoItem */
        $todoItem = $serializer->deserialize($request->getContent(), ToDoItem::class, 'json', ['groups' => ['create']]);
        $todoList = $todoApp->createToDoItem($todoItem);

        return $this->json($todoList, Response::HTTP_CREATED, [], ['groups' => ['details']]);
    }

    /**
     * @Route(path="/{id}", name="todo-item-complete", methods={"put"})
     *
     * @param ToDoItem         $item
     * @param ToDoAppInterface $todoApp
     *
     * @return JsonResponse
     */
    public function complete(ToDoItem $item, ToDoAppInterface $todoApp)
    {
        $todoApp->markToDoItemCompleted($item);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
