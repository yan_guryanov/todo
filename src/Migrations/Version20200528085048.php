<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

final class Version20200528085048 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create table for todo list';
    }

    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('todo_items');
        $table->addColumn('id', Types::INTEGER, ['autoincrement' => true, 'notnull' => true]);
        $table->addColumn('title', Types::STRING, ['length' => 255, 'notnull' => true]);
        $table->addColumn('completed', Types::BOOLEAN, ['notnull' => true]);
        $table->addColumn('created_for_date', Types::DATE_MUTABLE, ['notnull' => true]);
        $table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable('todo_items');
    }
}
