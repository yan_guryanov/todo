$(function(){
    let box = $('.js-todo-list');
    let spinnerBox = $('.js-spinner');

    let reloadData = function () {
        $.ajax({
            url: box.data('url'),
            dataType: 'json',
            method: 'get',
            contentType: 'application/json',
            success: function (data) {
                spinnerBox.hide();
                if (!'items' in data) {
                    box.prepend(
                        '<li class="list-group-item js-list-group-item">' +
                        '<div class="text-center">No Items For Today!</div>' +
                        '</li>'
                    );

                    return;
                }

                $.each(data['items'], function (i, item) {
                    let elem = $('<li>')
                        .addClass('list-group-item')
                        .addClass('js-list-group-item');

                    box.append(elem);

                    if (item.completed) {
                        elem.append($('<div>').html('<s>' + item.title + '</s>'));
                    } else {
                        elem
                            .addClass('list-group-item-action')
                            .addClass('js-list-group-item-action')
                            .append($('<div>').html(item.title))
                            .data('url', item.complete_url);
                    }
                });
                box.append(spinnerBox);
            },
            error: function () {
                spinnerBox.hide();
                box.prepend(
                    '<li class="list-group-item list-group-item-danger js-list-group-item">' +
                        '<div class="text-center">Some error occurred!</div>' +
                    '</li>'
                );
            }
        });
    };

    reloadData();

    let getTodayAsString = function () {
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0'+dd;
        }
        if (mm < 10) {
            mm = '0'+mm;
        }

        return yyyy + '-' + mm + '-' + dd;
    };

    // init modal
    let modalTitle = $('.js-title');
    let modalCreatedForDate = $('.js-created-for-date');
    let modal = $('#todoModal');
    modal.on('show.bs.modal', function (e) {
        modalTitle.val('');
        let todayString = getTodayAsString();
        modalCreatedForDate.val(todayString).attr('min', todayString);
    });

    // save data from modal
    let saveBtn = $('.js-save-item');

    saveBtn.on('click', function () {
        let data = {
            title: modalTitle.val(),
            created_for_date: modalCreatedForDate.val()
        };
        modal.modal('hide');
        box.find('.js-list-group-item').remove();
        spinnerBox.show();
        $.ajax({
            url: saveBtn.data('url'),
            dataType: 'json',
            method: 'post',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function () {
                spinnerBox.hide();
                reloadData();
            },
            error: function () {
                spinnerBox.hide();
                box.prepend(
                    '<li class="list-group-item list-group-item-danger js-list-group-item">' +
                    '<div class="text-center">Some error occurred!</div>' +
                    '</li>'
                );
            }
        });
    });

    box.on('click', '.js-list-group-item-action', function (e) {
        let elem = $(this);
        let url = elem.data('url');
        $.ajax({
            url: url,
            dataType: 'json',
            method: 'put',
            contentType: 'application/json',
            success: function () {
                let div = elem.find('div');
                let text = div.html();
                div.html('<s>' + text + '</s>');
                elem
                    .data('url', null)
                    .removeClass('js-list-group-item-action')
                    .removeClass('list-group-item-action');
            },
            error: function () {
                box.find('.js-list-group-item').remove();
                box.prepend(
                    '<li class="list-group-item list-group-item-danger js-list-group-item">' +
                    '<div class="text-center">Some error occurred!</div>' +
                    '</li>'
                );
            }
        });
    });
});
