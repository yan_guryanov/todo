## How To
### Prerequisites

* [Docker](https://docs.docker.com/install/)
* [Docker Compose](https://docs.docker.com/install/)


### Using the `run` util
To bootstrap and start services:

```bash
./run start
```

To stop services:

```bash
./run stop
```

To restart services:

```bash
./run restart
```

To cleanup:

```bash
./run clean
```

Update your hosts file with 127.0.0.1 test.local 

Open in your browser http://test.local:8088 
